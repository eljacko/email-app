import React from 'react';
import { connect, useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import TableRow from '@material-ui/core/TableRow';
import {
    Button,
    makeStyles,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
} from '@material-ui/core';

import EnhancedTableHead from './EnhancedTableHead';
import * as messageActions from '../actions/messages';
import { store } from '../Store';

function mapStateToProps({ messages }) {
    return { messages };
}
const mapDispatchToProps = (dispatch) => bindActionCreators({ ...messageActions }, dispatch);

const useStyles = makeStyles((theme) => ({
    paper: {
        marginBottom: theme.spacing(2),
        width: '100%',
    },
    root: {
        width: '100%',
    },
}));

const headCells = [
    { id: 'message_id', label: 'Message id', sortable: true },
    { id: 'receiver_address', label: 'Receiver Address', sortable: true },
    { id: 'email_subject', label: 'Email Subject', sortable: true },
    { id: 'email_message', label: 'Email Message', sortable: true },
    { id: 'delivered_time', label: 'Delivered Time', sortable: true },
    { id: 'delete', label: 'Delete', sortable: true },
];

// eslint-disable-next-line no-unused-vars
const Mailbox = ({ messages }) => {
    const dispatch = useDispatch();
    const classes = useStyles();

    React.useEffect(() => {
        if (!messages.messagesList.length) {
            store.dispatch(messageActions.fetchMessages());
        }
    }, [messageActions.fetchMessages]);

    const deleteMessage = (messageId) => {
        dispatch(messageActions.deleteMessage({ messageId }));
    };

    const handleRequestSort = (event, property) => {
        const isAsc = messages.orderBy === property && messages.order === 'asc';
        dispatch(messageActions.sortMessages({ order: isAsc ? 'desc' : 'asc', orderBy: property }));
    };

    return (
        <div className={classes.root}>
            {!!messages.messagesList.length && (
                <div style={{ marginTop: 20 }}>
                    <TableContainer component={Paper} style={{ padding: 5 }}>
                        <Table aria-label="simple table" className={classes.table}>
                            <EnhancedTableHead
                                classes={classes}
                                headCells={headCells}
                                onRequestSort={handleRequestSort}
                                order={messages.order}
                                orderBy={messages.orderBy}
                            />
                            <TableBody>
                                {messages.messagesList.map((message) => (
                                    <TableRow key={message.id}>
                                        <TableCell align="center">{message.id}</TableCell>
                                        <TableCell align="center">
                                            {message.receiverAddress}
                                        </TableCell>
                                        <TableCell align="center">{message.emailSubject}</TableCell>
                                        <TableCell align="center">{message.emailMessage}</TableCell>
                                        <TableCell align="center">
                                            {message.deliveredTime}
                                        </TableCell>
                                        <TableCell>
                                            <Button
                                                onClick={() => {
                                                    deleteMessage(message.id);
                                                }}
                                                value={message.id}
                                                variant="contained"
                                            >
                                                Delete message
                                            </Button>
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </div>
            )}
            {!messages.messagesList && <div>Mailbox is empty</div>}
        </div>
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(Mailbox);
