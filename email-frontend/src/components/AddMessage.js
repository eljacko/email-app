import React from 'react';
import { connect, useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Button, Grid, makeStyles, TextareaAutosize, TextField } from '@material-ui/core';

import { addMessage as addMessageAction } from '../actions/messages';

function mapStateToProps({ messages }) {
    return { messages };
}

const mapDispatchToProps = (dispatch) =>
    bindActionCreators({ closeMenu: addMessageAction }, dispatch);

const useStyles = makeStyles((theme) => ({
    paper: {
        marginBottom: theme.spacing(2),
        width: '100%',
    },
    root: {
        width: '100%',
    },
    textArea: {
        width: 400,
    },
    textField: {
        width: 400,
    },
}));

// eslint-disable-next-line no-unused-vars
const AddMessage = ({ messages }) => {
    const dispatch = useDispatch();
    const classes = useStyles();

    const [receiverAddress, setReceiverAddress] = React.useState('');
    const [emailSubject, setEmailSubject] = React.useState('');
    const [emailMessage, setEmailMessage] = React.useState('');

    const handleChangeReceiverAddress = (event) => {
        setReceiverAddress(event.target.value);
    };
    const handleChangeEmailSubject = (event) => {
        setEmailSubject(event.target.value);
    };
    const handleChangeEmailMessage = (event) => {
        setEmailMessage(event.target.value);
    };

    const submitMessage = () => {
        dispatch(addMessageAction({ emailMessage, emailSubject, receiverAddress }));
    };

    return (
        <div className={classes.root}>
            <Grid item xs={12}>
                <Grid container justify="center" spacing={2}>
                    <Grid item>
                        <TextField
                            className={classes.textField}
                            id="receiver-address"
                            label="Receiver Address"
                            onChange={handleChangeReceiverAddress}
                            value={receiverAddress}
                            variant="outlined"
                        />
                    </Grid>
                </Grid>
                <Grid container justify="center" spacing={2}>
                    <Grid item>
                        <TextField
                            className={classes.textField}
                            id="email-subject"
                            label="Email Subject"
                            onChange={handleChangeEmailSubject}
                            value={emailSubject}
                            variant="outlined"
                        />
                    </Grid>
                </Grid>
                <Grid container justify="center" spacing={2}>
                    <Grid item>
                        <TextareaAutosize
                            aria-label="email message"
                            className={classes.textArea}
                            id="email-message"
                            label="Email Message"
                            onChange={handleChangeEmailMessage}
                            rowsMin={4}
                            value={emailMessage}
                            variant="outlined"
                        />
                    </Grid>
                </Grid>
                <Grid container justify="center" spacing={2}>
                    <Grid item>
                        <Button onClick={submitMessage} variant="contained">
                            Add message
                        </Button>
                    </Grid>
                </Grid>
            </Grid>
        </div>
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(AddMessage);
