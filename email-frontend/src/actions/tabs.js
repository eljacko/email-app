/* eslint-disable import/prefer-default-export */

export function tabChange(payload) {
    return { payload, type: 'TAB_CHANGE' };
}
