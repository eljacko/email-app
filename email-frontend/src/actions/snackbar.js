/* eslint-disable import/prefer-default-export */

export function openSnackbar(payload) {
    return { payload, type: 'SNACKBAR_OPEN' };
}

export function closeSnackbar(payload) {
    return { payload, type: 'SNACKBAR_CLOSE' };
}
