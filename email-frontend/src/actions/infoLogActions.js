export const infoLogMessageReceived = (payload) => ({
    payload,
    type: 'INFOLOG_MESSAGE_RECEIVED',
});

export const addInfolog = (payload) => ({
    payload,
    type: 'ADD_INFOLOG',
});
