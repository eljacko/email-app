import { find } from 'lodash';

function messagesRequest(requestParams, payload) {
    return { payload, requestParams, type: 'MESSAGES_REQUEST' };
}

function messagesSuccess(requestParams, payload) {
    return { payload, requestParams, type: 'MESSAGES_SUCCESS' };
}

function messagesFailure(requestParams, payload) {
    return { payload, requestParams, type: 'MESSAGES_FAILURE' };
}

function addMessageRequest(requestParams, payload) {
    return { payload, requestParams, type: 'ADD_MESSAGE_REQUEST' };
}

function addMessageSuccess(requestParams, payload) {
    return { payload, requestParams, type: 'ADD_MESSAGE_SUCCESS' };
}

function addMessageFailure(requestParams, payload) {
    return { payload, requestParams, type: 'ADD_MESSAGE_FAILURE' };
}

function deleteMessageRequest(requestParams, payload) {
    return { payload, requestParams, type: 'DELETE_MESSAGE_REQUEST' };
}

function deleteMessageSuccess(requestParams, payload) {
    return { payload, requestParams, type: 'DELETE_MESSAGE_SUCCESS' };
}

function deleteMessageFailure(requestParams, payload) {
    return { payload, requestParams, type: 'DELETE_MESSAGE_FAILURE' };
}

export function fetchMessages(params) {
    return (dispatch, getState, { axios }) => {
        const requestParams = {
            method: 'get',
            url: `/v1/messages`,
        };

        if (find(getState().requests, requestParams)) {
            return Promise.resolve(true);
        }

        dispatch(messagesRequest(requestParams, params));

        return axios
            .request(requestParams)
            .then(({ data }) => dispatch(messagesSuccess(requestParams, data.messages)))
            .catch(({ response, message }) => {
                if (response && response.data) {
                    return dispatch(messagesFailure(requestParams, response.data));
                }
                return dispatch(messagesFailure(requestParams, { message }));
            })
            .then(() => true);
    };
}

export function addMessage(params) {
    return (dispatch, getState, { axios }) => {
        const { emailMessage, emailSubject, receiverAddress } = params;

        const requestParams = {
            data: { emailMessage, emailSubject, receiverAddress },
            method: 'post',
            url: '/v1/messages',
        };

        if (find(getState().requests, requestParams)) {
            return Promise.resolve(true);
        }

        dispatch(addMessageRequest(requestParams, params));

        return axios
            .request(requestParams)
            .then(({ data }) => dispatch(addMessageSuccess(requestParams, data)))
            .catch(({ response, message }) => {
                if (response && response.data) {
                    return dispatch(addMessageFailure(requestParams, response.data));
                }
                return dispatch(addMessageFailure(requestParams, { message }));
            })
            .then(() => true);
    };
}

export function deleteMessage(params) {
    return (dispatch, getState, { axios }) => {
        const { messageId } = params;

        const requestParams = {
            method: 'delete',
            url: `/v1/message/${messageId}`,
        };

        if (find(getState().requests, requestParams)) {
            return Promise.resolve(true);
        }

        dispatch(deleteMessageRequest(requestParams, params));

        return axios
            .request(requestParams)
            .then(() => dispatch(deleteMessageSuccess(requestParams, { messageId })))
            .catch(({ response, message }) => {
                if (response && response.data) {
                    return dispatch(deleteMessageFailure(requestParams, response.data));
                }
                return dispatch(deleteMessageFailure(requestParams, { message }));
            })
            .then(() => true);
    };
}

export function sortMessages(payload) {
    return { payload, type: 'SORT_MESSAGES' };
}
