import React from 'react';
import { connect, useDispatch } from 'react-redux';
import MuiAlert from '@material-ui/lab/Alert';
import PropTypes from 'prop-types';
import { AppBar, Box, Snackbar, Tab, Tabs, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import './App.css';
import * as infoLogActions from './actions/infoLogActions';
import { closeSnackbar } from './actions/snackbar';
import { tabChange } from './actions/tabs';
import AddMessage from './components/AddMessage';
import Mailbox from './components/Mailbox';

function mapStateToProps({ infolog, snackbar }) {
    return { infolog, snackbar };
}

const mapDispatchToProps = { ...infoLogActions };

const getLogStyle = (line) => {
    if (line.startsWith('DEBUG')) {
        return 'logDebug';
    }
    if (line.startsWith('ERROR')) {
        return 'logError';
    }
    if (line.startsWith('WARN')) {
        return 'logWarn';
    }
    return 'logInfo';
};

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <Typography
            aria-labelledby={`simple-tab-${index}`}
            component="div"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            role="tabpanel"
            {...other}
        >
            {value === index && <Box p={3}>{children}</Box>}
        </Typography>
    );
}

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

TabPanel.propTypes = {
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        'aria-controls': `full-width-tabpanel-${index}`,
        id: `full-width-tab-${index}`,
    };
}

const useStyles = makeStyles((theme) => ({
    indicator: {
        backgroundColor: '#D3D3D3',
    },
    root: {
        backgroundColor: theme.palette.background.paper,
        flexGrow: 1,
    },
    snackbarIcon: {
        marginRight: 10,
        verticalAlign: 'middle',
    },
}));

function App({ infolog, snackbar }) {
    const dispatch = useDispatch();
    const classes = useStyles();
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
        dispatch(tabChange());
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        dispatch(closeSnackbar({ open: false }));
    };

    return (
        <div className={`App ${classes.root}`}>
            <AppBar position="static">
                <Tabs aria-label="main menu" onChange={handleChange} value={value}>
                    <Tab label="Add Messages" {...a11yProps(0)} />
                    <Tab label="Mailbox" {...a11yProps(1)} />
                </Tabs>
            </AppBar>
            <TabPanel index={0} value={value}>
                <AddMessage />
            </TabPanel>
            <TabPanel index={1} value={value}>
                <Mailbox />
            </TabPanel>
            <div className="messageBox">
                {infolog.loglines
                    .slice(0)
                    .reverse()
                    .map((line, i) => (
                        <div key={''.concat(i, line.substring(0, 1))} className={getLogStyle(line)}>
                            {line}
                        </div>
                    ))}
            </div>
            <Snackbar autoHideDuration={6000} onClose={handleClose} open={snackbar.open}>
                <Alert onClose={handleClose} severity={snackbar.severity}>
                    {snackbar.message}
                    {snackbar.error}
                </Alert>
            </Snackbar>
        </div>
    );
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
