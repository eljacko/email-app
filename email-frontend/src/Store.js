import { compose, createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import axios from 'axios';
import rootReducer from './reducers/rootReducer';
// eslint-disable-next-line no-underscore-dangle
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const axiosConfig = {
    baseURL: process.env.REACT_APP_PUBLIC_API_URL,
    headers: {
        'Access-Control-Allow-Methods': 'GET,POST,DELETE',
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
    },
    timeout: 60 * 1000,
};

const middleware = [
    thunk.withExtraArgument({
        axios: axios.create(axiosConfig),
    }),
];

export const store = createStore(rootReducer, composeEnhancers(applyMiddleware(...middleware)));
export { store as default };
