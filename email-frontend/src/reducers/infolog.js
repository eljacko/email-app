const defaultState = {
    loglines: [],
};

// to keep it under control
const maxLogArrayLength = 99;

export default (state = defaultState, action) => {
    if (action.type === 'INFOLOG_MESSAGE_RECEIVED') {
        const logline = ''.concat(
            action.payload.type,
            ' : ',
            action.payload.className,
            ' - ',
            action.payload.message
        );

        if (state.loglines.length > maxLogArrayLength) {
            state.loglines.shift();
        }

        return { ...state, loglines: state.loglines.concat([logline]) };
    }

    if (action.type === 'ADD_INFOLOG') {
        if (state.loglines.length > maxLogArrayLength) {
            state.loglines.shift();
        }

        return { ...state, loglines: state.loglines.concat([action.payload]) };
    }

    return state;
};
