import { combineReducers } from 'redux';
import messages from './messages';
import infolog from './infolog';
import snackbar from './snackbar';

export default combineReducers({
    infolog,
    messages,
    snackbar,
});
