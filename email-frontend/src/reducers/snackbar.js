const initialState = {
    error: '',
    message: '',
    open: false,
    severity: 'success',
};

function reducer(state = initialState, action) {
    if (action.type === 'SNACKBAR_CLOSE') {
        return { ...initialState };
    }
    if (action.type === 'SNACKBAR_OPEN') {
        return {
            ...state,
            message: action.payload.message,
            open: true,
            severity: action.payload.severity,
        };
    }
    if (action.type === 'ADD_CUSTOMER_FAILURE') {
        return {
            ...state,
            error: action.payload.message,
            message: 'Adding customer failed: ',
            open: true,
            severity: 'error',
        };
    }
    if (action.type === 'CUSTOMERS_FAILURE') {
        return { ...state, message: 'Failed to fetch customers', open: true, severity: 'error' };
    }
    if (action.type === 'ADD_CUSTOMERS_SUCCESS') {
        return { ...state, message: 'Customer added', open: true, severity: 'success' };
    }
    return state;
}

export default reducer;
