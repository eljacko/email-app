import { getComparator, stableSort } from '../utils/tableSort';

const initialState = {
    loading: false,
    messagesList: [],
    order: 'asc',
    orderBy: 'receiverAddress',
};

export default (state = initialState, action) => {
    if (
        action.type === 'ADD_MESSAGE_REQUEST' ||
        action.type === 'DELETE_MESSAGE_REQUEST' ||
        action.type === 'MESSAGES_REQUEST'
    ) {
        return { ...state, loading: true };
    }

    if (
        action.type === 'ADD_MESSAGE_FAILURE' ||
        action.type === 'DELETE_MESSAGE_FAILURE' ||
        action.type === 'MESSAGES_FAILURE'
    ) {
        return { ...state, loading: false };
    }

    if (action.type === 'MESSAGES_SUCCESS') {
        return {
            ...initialState,
            messagesList: stableSort(action.payload, getComparator(state.order, state.orderBy)),
        };
    }

    if (action.type === 'ADD_MESSAGE_SUCCESS') {
        const newState = { ...state, loading: false };
        newState.messagesList.push(action.payload);
        newState.messagesList = stableSort(
            state.messagesList,
            getComparator(state.order, state.orderBy)
        );
        return newState;
    }

    if (action.type === 'DELETE_MESSAGE_SUCCESS') {
        const newState = { ...state, loading: false };
        newState.messagesList = newState.messagesList.filter(
            (message) => message.id !== action.payload.messageId
        );
        return newState;
    }

    return state;
};
