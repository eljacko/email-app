## Task description

Please write sample application with the following steps using Java with Spring Boot and ReactJS with Redux frameworks:
 
1. Rest endpoint that receives email receiver address, email subject and email message. Store this message to DB.
2. After one hour retrieve all unsent messages from DB and send them out.
3. Create UI with React to call this endpoint.

## Running application with docker

Most convinient way to run application is with docker, using command:

    docker-compose -f docker-compose.yml up

On one build db failed to start before backend application, making latter to fail. In that case, just stop continers and start again.

To rebuild and run

    docker-compose -f docker-compose.yml up --build


## Developing and setting up 

Instructions on setting up and running frontend and backend applications are included in projects readme files.

## Mail client

In default config, I'm using gmail to send (and receive) messages, but this can be changed through application properties. If getting error on connecting to gmail, reallowing less secure apps may be needed. Login details in `application.properties` file.

    https://myaccount.google.com/lesssecureapps
