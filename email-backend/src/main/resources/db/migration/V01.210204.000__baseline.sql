--
-- PostgreSQL database dump
--

-- Dumped from database version 13.1
-- Dumped by pg_dump version 13.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: messages; Type: TABLE; Schema: public; Owner: omniva
--

CREATE TABLE public.messages (
                                 id bigint NOT NULL,
                                 delivered_time timestamp without time zone,
                                 email_message character varying(1024),
                                 email_subject character varying(128),
                                 receiver_address character varying(64)
);


ALTER TABLE public.messages OWNER TO omniva;

--
-- Name: messages_id_seq; Type: SEQUENCE; Schema: public; Owner: omniva
--

CREATE SEQUENCE public.messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.messages_id_seq OWNER TO omniva;

--
-- Data for Name: messages; Type: TABLE DATA; Schema: public; Owner: omniva
--

COPY public.messages (id, delivered_time, email_message, email_subject, receiver_address) FROM stdin;
\.


--
-- Name: messages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: omniva
--

SELECT pg_catalog.setval('public.messages_id_seq', 1, false);


--
-- Name: messages messages_pkey; Type: CONSTRAINT; Schema: public; Owner: omniva
--

ALTER TABLE ONLY public.messages
    ADD CONSTRAINT messages_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

