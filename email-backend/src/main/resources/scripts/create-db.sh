#!/bin/bash

psql postgres -c "CREATE ROLE omniva WITH LOGIN PASSWORD 'ajutine123';"
psql postgres -c "CREATE DATABASE omniva WITH OWNER omniva
    ENCODING 'UTF8' LC_COLLATE 'C' LC_CTYPE 'C' TEMPLATE template0;"
psql postgres -c "GRANT ALL PRIVILEGES ON DATABASE omniva TO omniva;"
