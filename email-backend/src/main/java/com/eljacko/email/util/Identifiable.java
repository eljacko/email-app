package com.eljacko.email.util;

import java.io.Serializable;

public interface Identifiable<T extends Serializable> {
    T getId();
}
