package com.eljacko.email.constant;

public final class FieldsLength {

    public static final int MESSAGES_RECEIVER_ADDRESS = 64;
    public static final int MESSAGES_EMAIL_MESSAGE = 1024;
    public static final int MESSAGES_EMAIL_SUBJECT = 128;


    private FieldsLength() {
        throw new UnsupportedOperationException(
                "This is a constants class and cannot be instantiated");
    }
}
