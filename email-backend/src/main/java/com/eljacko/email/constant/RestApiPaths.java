package com.eljacko.email.constant;

public final class RestApiPaths {

    public static final String MESSAGES = "/messages";
    public static final String MESSAGE = "/message/{messageId}";


    private RestApiPaths() {
        throw new UnsupportedOperationException(
                "This is a constants class and cannot be instantiated");
    }
}
