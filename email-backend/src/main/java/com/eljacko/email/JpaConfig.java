package com.eljacko.email;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories("com.eljacko.email.repository")
@EntityScan(basePackages = "com.eljacko.email.entity")
@SuppressWarnings({ "checkstyle:DesignForExtension" })
public class JpaConfig {

}
