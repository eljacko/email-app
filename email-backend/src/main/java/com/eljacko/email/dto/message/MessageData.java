package com.eljacko.email.dto.message;

import com.eljacko.email.entity.Message;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@NoArgsConstructor
public class MessageData {
    private Long id;
    private String receiverAddress;
    private String emailSubject;
    private String emailMessage;
    private Timestamp deliveredTime;

    public MessageData(final Message message) {
        this.id = message.getId();
        this.receiverAddress = message.getReceiverAddress();
        this.emailSubject = message.getEmailSubject();
        this.emailMessage = message.getEmailMessage();
        this.deliveredTime = message.getDeliveredTime();
    }
}
