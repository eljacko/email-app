package com.eljacko.email.dto.message;

import com.eljacko.email.constant.ValidationMessages;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@NoArgsConstructor
public class MessageEntryData {

    @Email(message = ValidationMessages.INVALID_EMAIL)
    @Pattern(regexp = ".+@.+\\..+", message = ValidationMessages.INVALID_EMAIL)
    @NotNull(message = ValidationMessages.NOT_NULL)
    @NotBlank
    private String receiverAddress;

    @NotNull(message = ValidationMessages.NOT_NULL)
    private String emailSubject;

    @NotNull(message = ValidationMessages.NOT_NULL)
    private String emailMessage;

}
