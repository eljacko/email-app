package com.eljacko.email.controller;

import com.eljacko.email.constant.RestApiPaths;
import com.eljacko.email.dto.message.MessageData;
import com.eljacko.email.dto.message.MessageEntryData;
import com.eljacko.email.dto.message.MessageListResponse;
import com.eljacko.email.service.MessageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1")
@SuppressWarnings({ "checkstyle:magicnumber", "checkstyle:designforextension" })
public class MessageController {
    private final MessageService messageService;

    @PostMapping(RestApiPaths.MESSAGES)
    public HttpEntity<?> addMessage(@RequestBody @Valid final MessageEntryData messageEntryData) {
        log.info("Request to add new email message {}", messageEntryData);
        MessageData result = messageService.addMessage(messageEntryData);
        log.info("Added new message for sending {}", result);
        return new ResponseEntity<MessageData>(result, HttpStatus.OK);
    }

    @GetMapping(RestApiPaths.MESSAGE)
    public HttpEntity<?> getMessage(@PathVariable("messageId") final Long messageId) {
        log.info("Request to get message with id {}", messageId);
        MessageData result = messageService.getMessage(messageId);
        return new ResponseEntity<MessageData>(result, HttpStatus.OK);
    }

    @DeleteMapping(RestApiPaths.MESSAGE)
    public HttpEntity<?> deleteMessage(@PathVariable("messageId") final Long messageId) {
        log.info("Request to delete message with id {}", messageId);
        messageService.deleteMessage(messageId);
        return new ResponseEntity<MessageData>(HttpStatus.OK);
    }

    @GetMapping(RestApiPaths.MESSAGES)
    public HttpEntity<?> getMessage() {
        log.info("Request to get all messages");
        MessageListResponse result = messageService.getAllMessages();
        return new ResponseEntity<MessageListResponse>(result, HttpStatus.OK);
    }

}
