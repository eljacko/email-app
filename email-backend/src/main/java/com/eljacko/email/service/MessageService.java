package com.eljacko.email.service;

import com.eljacko.email.dto.message.MessageData;
import com.eljacko.email.dto.message.MessageEntryData;
import com.eljacko.email.dto.message.MessageListResponse;

public interface MessageService {

    MessageData addMessage(MessageEntryData messageEntryData);

    MessageData getMessage(Long messageId);

    void deleteMessage(Long messageId);

    MessageListResponse getAllMessages();

}
