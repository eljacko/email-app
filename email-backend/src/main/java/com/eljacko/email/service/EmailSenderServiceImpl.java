package com.eljacko.email.service;

import com.eljacko.email.entity.Message;
import com.eljacko.email.repository.MessageRepository;
import com.eljacko.email.service.util.DateUtilService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
@SuppressWarnings({"checkstyle:designforextension"})
public class EmailSenderServiceImpl implements EmailSenderService {

    private final MessageRepository messageRepository;
    private final EmailService emailService;
    private final DateUtilService dateUtilService;

    @Scheduled(cron = "@hourly")
    public void sendUndeliveredMessages() {
        log.info("Sending undelivered messages started at {}",
                dateUtilService.getCurrentTimeAsTimestamp());
        List<Message> messages = messageRepository.findByDeliveredTimeIsNull();
        messages.stream().forEach(message -> {
            emailService.prepareAndSendEmail(message);
            message.setDeliveredTime(dateUtilService.getCurrentTimeAsTimestamp());
            messageRepository.save(message);
        });
        log.info("Sending messages finished at {}",
                dateUtilService.getCurrentTimeAsTimestamp());
    }

}
