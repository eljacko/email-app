package com.eljacko.email.service;

import com.eljacko.email.entity.Message;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
@SuppressWarnings({"checkstyle:designforextension"})
public class EmailServiceImpl implements EmailService {

    private final JavaMailSender emailSender;

    @Value("${spring.mail.sender:}")
    private String fromEmail = null;

    @Override
    public void sendEmail(final SimpleMailMessage message) {
        emailSender.send(message);
    }

    @Override
    public void prepareAndSendEmail(final Message message) {
        emailSender.send(compileMessage(message));
    }

    private SimpleMailMessage compileMessage(final Message message) {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom(fromEmail);
        simpleMailMessage.setTo(message.getReceiverAddress());
        simpleMailMessage.setSubject(message.getEmailSubject());
        simpleMailMessage.setText(message.getEmailMessage());
        return simpleMailMessage;
    }
}
