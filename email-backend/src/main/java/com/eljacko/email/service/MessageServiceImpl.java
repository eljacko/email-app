package com.eljacko.email.service;

import com.eljacko.email.dto.message.MessageData;
import com.eljacko.email.dto.message.MessageEntryData;
import com.eljacko.email.dto.message.MessageListResponse;
import com.eljacko.email.entity.Message;
import com.eljacko.email.exception.NotFoundException;
import com.eljacko.email.repository.MessageRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
@SuppressWarnings({"checkstyle:designforextension"})
public class MessageServiceImpl implements MessageService {

    private final MessageRepository messageRepository;

    @Override
    @Transactional
    public MessageData addMessage(final MessageEntryData messageEntryData) {
        Assert.notNull(messageEntryData, "Message entry data must not be null");
        Message messageDb = new Message();
        messageDb = messageRepository.save(messageDb);
        messageDb.setReceiverAddress(messageEntryData.getReceiverAddress());
        messageDb.setEmailSubject(messageEntryData.getEmailSubject());
        messageDb.setEmailMessage(messageEntryData.getEmailMessage());

        return new MessageData(messageDb);
    }

    @Override
    @Transactional(readOnly = true)
    public MessageData getMessage(final Long messageId) {
        Message messageDb = messageRepository.findById(messageId)
                .orElseThrow(() -> new NotFoundException("Message not found"));
        return new MessageData(messageDb);
    }

    @Override
    @Transactional
    public void deleteMessage(final Long messageId) {
        messageRepository.deleteById(messageId);
    }

    @Override
    @Transactional(readOnly = true)
    public MessageListResponse getAllMessages() {
        List<Message> messagesDb = messageRepository.findAll();
        List<MessageData> messageDataList = messagesDb.stream()
                .map(message -> new MessageData(message))
                .collect(Collectors.toList());
        return new MessageListResponse(messageDataList);
    }
}
