package com.eljacko.email.service;

import com.eljacko.email.entity.Message;
import org.springframework.mail.SimpleMailMessage;

public interface EmailService {

    void sendEmail(SimpleMailMessage message);

    void prepareAndSendEmail(Message message);

}
