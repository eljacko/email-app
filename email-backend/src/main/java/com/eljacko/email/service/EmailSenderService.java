package com.eljacko.email.service;

public interface EmailSenderService {

    void sendUndeliveredMessages();

}
