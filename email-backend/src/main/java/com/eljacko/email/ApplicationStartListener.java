package com.eljacko.email;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.event.ContextRefreshedEvent;

@Slf4j
@Configuration
@PropertySource(value = "classpath:version.properties", ignoreResourceNotFound = true)
public class ApplicationStartListener implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private ConfigurableApplicationContext context;

    @Autowired
    private WebSecurityConfig webConfig;


    @Override
    public final void onApplicationEvent(final ContextRefreshedEvent event) {
        boolean shutDown = false;
        if (log.isInfoEnabled()) {
            log.info("Verifying configuration parameters...");
        }

        if (webConfig.getFrontendPublicUrl() == null
                || webConfig.getFrontendPublicUrl().length() < 2) {
            log.error("There is no email-api.frontend.public-url value in "
                    + "configuration file!");
            shutDown = true;
        }

        if (shutDown) {
            SpringApplication.exit(context);
        } else {
            if (log.isInfoEnabled()) {
                log.info("Configuration OK");
                log.info("Starting email application");
            }
        }
    }
}
