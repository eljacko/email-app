package com.eljacko.email.entity;

import com.eljacko.email.constant.FieldsLength;
import com.eljacko.email.constant.ValidationMessages;
import com.eljacko.email.entity.base.BaseEntity;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.sql.Timestamp;

@Setter
@ToString(callSuper = true)
@Entity
@Table(name = "messages")
@SuppressWarnings({ "checkstyle:MemberName", "checkstyle:MethodName",
        "checkstyle:DesignForExtension" })
//@formatter:off
@GenericGenerator(
        name = "assigned-sequence",
        strategy = "com.eljacko.email.util.AssignedSequenceStyleGenerator",
        parameters = {
                @org.hibernate.annotations.Parameter(
                        name = "sequence_name",
                        value = "messages_id_seq"),
                @org.hibernate.annotations.Parameter(
                        name = "optimizer",
                        value = "none")
        })
//@formatter:on
public class Message extends BaseEntity {
    private static final long serialVersionUID = 1L;

    private String receiverAddress;
    private String emailSubject;
    private String emailMessage;
    private Timestamp deliveredTime;

    public Message() {
        super();
    }

    /** Email receiver address */
    @Size(max = FieldsLength.MESSAGES_RECEIVER_ADDRESS,
            message = ValidationMessages.MAX_LENGTH_EXCEEDED)
    @Column(length = FieldsLength.MESSAGES_RECEIVER_ADDRESS)
    public String getReceiverAddress() {
        return receiverAddress;
    }

    /** Subject of the email */
    @Size(max = FieldsLength.MESSAGES_EMAIL_SUBJECT,
            message = ValidationMessages.MAX_LENGTH_EXCEEDED)
    @Column(length = FieldsLength.MESSAGES_EMAIL_SUBJECT)
    public String getEmailSubject() {
        return emailSubject;
    }

    /** Message body of the email */
    @Size(max = FieldsLength.MESSAGES_EMAIL_MESSAGE,
            message = ValidationMessages.MAX_LENGTH_EXCEEDED)
    @Column(length = FieldsLength.MESSAGES_EMAIL_MESSAGE)
    public String getEmailMessage() {
        return emailMessage;
    }

    /** Record delivered time timestamp */
    @Column(name = "delivered_time")
    public Timestamp getDeliveredTime() {
        return deliveredTime;
    }

}
