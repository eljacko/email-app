## Building

Gradle is used for building the project and compiling a distribution archive. 
To build everything:

    ./gradlew build

For running on your computer use:

    ./gradlew clean build :bootRun
    
To run the application without first building an archive use the bootRun task:
    
    ./gradlew :bootRun

To build spring-boot jar:

    ./gradlew clean bootJar
    
By default, application uses 8088 port, to change port put in application.properties:

	server.port=8088

## Configuration
There should be 2 configurations files to run an application on a developer's computer:

	application.properties (additional)
	log4j2.xml

#### Mail client
I did a test gmail to send (and receive) messages. As google turns off access for this application 
from time to time, it may be necessary to allow access for less secure applications from here. 

    https://myaccount.google.com/lesssecureapps

#### All properties and logging defaults are set, to run on developers' computer
	
#### Additional `application.properties` file
Default values for the email application are provided in a `application.properties` file in the 
location `src/main/resources`. This file is called as **default** `application.properties` file. 
To run email application you need to create an additional `application.properties` file where you 
define environment values. This file is called as **additional** `application.properties` file. 
A template for the additional file is located in application directory as 
`application.properties.additional.sample`, there are the minimum values that you need to run 
the application. Put your additional `application.properties` file to the application root directory 
to load configuration properties automatically. If there is need to change values defined in the 
default file, then you can do it by putting them to your additional `application.properties` file. 
All values are defined in the additional are override values from the default `application.properties` file.

#### `log4j2.xml`
For logging, you need to create a file `log4j2.xml`. A template for the file is located in 
`src/main/resources` `log4j2.xml.sample` Put the created file to the application module root 
directory or to `src/main/resources` to load logging configuration automatically.

### Creating databases
Application needs database to run, database creation script `create-db.sh` is located in`resources`.
Script has default postgres user with no password, if changed modify accordingly.
To run from project directory:

    sh ./src/main/resources/scripts/create-db.sh 
